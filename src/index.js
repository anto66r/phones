import React from 'react';
import { render } from 'react-dom';
import App from 'containers/app'
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import reducer from 'reducers';
import thunk from 'redux-thunk'
const middleware = [ thunk ];

const store = createStore(
  reducer,
  applyMiddleware(...middleware)
)

render(
  <Provider store={ store }>
    <App />
  </Provider>,
  document.getElementById('app')
);
