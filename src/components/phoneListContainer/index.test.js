import React from "react";
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import PhoneListContainer from "./";

describe("PhoneListContainer", () => {
  let props;
  const wrapper = () => {
    return shallow(
      <PhoneListContainer {...props} />
    );
  };

  beforeEach(() => {
    props = {
      popup: {
        isPopupOpen: false
      },
      phones: {
        loading: false,
        phones: []
      },
      actions: {
        popup: {
          open: jest.fn()
        },
        phones: {
          load: jest.fn()
        }
      }
    };
  });

  it("+++snapshot, no popup, empty phones array, no loading", () => {
    expect(toJson(wrapper())).toMatchSnapshot();
  });
  it("+++snapshot, 2 phones", () => {
    props.phones.phones=[
      {
        id: "aa",
        name: "bb",
        price: 12
      },
      {
        id: "bb",
        name: "bb",
        price: 12
      }
    ]
    expect(toJson(wrapper())).toMatchSnapshot();
  });
  it("+++snapshot, popup", () => {
    props.isPopupOpen = true;
    expect(toJson(wrapper())).toMatchSnapshot();
  });
  it("+++snapshot, loading", () => {
    props.phones.loading = true;
    expect(toJson(wrapper())).toMatchSnapshot();
  });
});
