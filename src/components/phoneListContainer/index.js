import React from 'react';
import PropTypes from 'prop-types';

import PhoneListItem from 'components/phoneListItem';
import Loading from 'components/loading';
import PhoneDetailContainer from 'containers/phoneDetailContainer';

import styles from './styles.css';
import './transition.css';

const doPhones = (phones, openPopup) => phones.map(el => (
  <PhoneListItem { ...el } key={ el.id } openPopup={ openPopup }/>
))

class PhoneListContainer extends React.Component {

  componentDidMount = () => {
    this.props.actions.phones.load()
  }

  render () {
    const { popup, phones=[], actions } = this.props;
    return (
      <div className={ styles.phoneListContainer }>
        {
            popup.isPopupOpen &&
          <PhoneDetailContainer phoneId={ popup.id } />
        }
        {
            phones.loading &&
          <Loading />
        }
        {
          !phones.error ?
            <div className={ styles.cards }>{ doPhones(phones.phones, actions.popup.open) }</div> :
            <div className={ styles.error }>{ phones.error }</div>
        }
        <div className={ styles.button }>
          <button onClick={ actions.phones.load } data-test="reload" >Reload</button>
        </div>
      </div>
    )
  }
}

PhoneListContainer.propTypes = {
  popup: PropTypes.object.isRequired,
  phones: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
}

export default PhoneListContainer;
