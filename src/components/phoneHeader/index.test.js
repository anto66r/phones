import React from "react";
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import PhoneHeader from "./";

describe("PhoneHeader", () => {
  let props;
  const wrapper = () => {
    return shallow(
      <PhoneHeader {...props} />
    );
  };

  beforeEach(() => {
    props = {
      title: ""
    };
  });

  it("+++snapshot, no title", () => {
    expect(toJson(wrapper())).toMatchSnapshot();
  });
  it("+++snapshot", () => {
    props.title = "aa"
    expect(toJson(wrapper())).toMatchSnapshot();
  });
});
