import React from 'react';
import PropTypes from 'prop-types';

import styles from './styles.css'

const PhoneHeader = ({ title }) => (
  <header className={ styles.header }>
    <div className={ styles.title }>{ title }</div>
  </header>
)

PhoneHeader.propTypes = {
  title: PropTypes.string
};

PhoneHeader.defaultProps = {
  title: "My Title"
};

export default PhoneHeader;
