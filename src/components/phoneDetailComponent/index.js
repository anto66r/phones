import React from 'react';
import Overlay from 'components/overlay';

import Price from 'components/price';
import styles from './styles.css';

const doColors = colors => colors.map((el, i) =>
  <div key={ i } style={ {backgroundColor: el} } className={ styles.colorbox } ></div>
)

const PhoneDetailComponent = (props) => {
  const phone = props.phones[props.phoneId]
  return (
    <div className={ styles.popup } data-test="detail" data-phoneid={ props.phoneId }>
      <button data-test="close" className={ styles.close } onClick={ props.actions.close }>Close</button>
      <div className={ styles.name }>{ phone.name }</div>
      <div className={ styles.price }><Price price={ phone.price } /></div>
      <div className={ styles.title }>Available colors</div>
      <div className={ styles.colors }>{ doColors(phone.colors) }</div>
      {
        phone.images &&
        <div>
          <div className={ styles.title }>Images</div>
          <div className={ styles.images }>
              {
                phone.images.map((el, i) => <img src={ el } key={ i }/>)
              }
          </div>
        </div>
      }
      <div className={ styles.title }>Description</div>
      <div>{ phone.description }</div>
    </div>
  )
}

export default Overlay(PhoneDetailComponent)
