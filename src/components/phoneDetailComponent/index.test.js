import React from "react";
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import configureStore from 'redux-mock-store';

import PhoneDetailComponent from "./";

describe("PhoneDetailComponent", () => {
  let store;
  const initialState = {
    id: "aa",
    name: "aa",
    thumbnail: "aa",
    images: [
      "aa",
      "aa"
    ],
    description: "aa",
    colors: ["aa"],
    price: 0.0
  };
  const mockStore = configureStore();
  const wrapper = () => {
    return shallow(
      <PhoneDetailComponent store={ store } />
    );
  };

  beforeEach(() => {
    store = mockStore(initialState);
  });

  it("+++snapshot", () => {
    expect(toJson(wrapper())).toMatchSnapshot();
  });
});
