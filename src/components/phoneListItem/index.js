import React from 'react';
import PropTypes from 'prop-types';

import Price from 'components/price';
import styles from './styles.css';
import noImg from './assets/noImage.png';

const thumb = (thumbnail) => {
  let thumb = thumbnail || noImg
  return (
    <div className={ styles.thumb }><img src={ thumb } /></div>
  )
}

const PhoneListItem = ({ name, price, thumbnail, id, openPopup }) => {
  const clickHandler = () => openPopup(id);

  return (
    <div className={ styles.card } onClick={ clickHandler } data-test="item" data-phoneid={ id }>
      <div className={ styles.name }>{ name }</div>
      { thumb(thumbnail) }
      <div className={ styles.price }><Price price={ price } /></div>
    </div>
  )
}

PhoneListItem.propTypes = {
  openPopup: PropTypes.func.isRequired,
  thumbnail: PropTypes.string,
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired
};

export default PhoneListItem;
