import React from "react";
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import PhoneListItem from "./";

describe("PhoneListItem", () => {
  let props;
  const wrapper = () => {
    return shallow(
      <PhoneListItem {...props} />
    );
  };
  beforeEach(() => {
    props = {
      openPopup: jest.fn(),
      id: "aa",
      name: "aa",
      price: 123
    }
  });
  it("+++snapshot, no image", () => {
    expect(toJson(wrapper())).toMatchSnapshot();
  });
  it("+++snapshot", () => {
    props.thumbnail = "aa";
    expect(toJson(wrapper())).toMatchSnapshot();
  });
});
