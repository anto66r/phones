import React from "react";
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import Price from "./";

describe("Price", () => {
  it("+++snapshot, 15.5 value", () => {
    const renderedValue = shallow(<Price price={ 15.5 } />);
    expect(toJson(renderedValue)).toMatchSnapshot();
  });
  it("+++snapshot, undefined value", () => {
    const renderedValue = shallow(<Price  />);
    expect(toJson(renderedValue)).toMatchSnapshot();
  });
});
