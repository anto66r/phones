import React from 'react';
import PropTypes from 'prop-types';

const Price = ({ price }) =>
  <span>
    {
      price ?
        price.toFixed(2) :
        "-"
    } €
  </span>

Price.propTypes = {
  price: PropTypes.number
};

export default Price;
