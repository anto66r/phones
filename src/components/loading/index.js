import React from 'react';
import Overlay from 'components/overlay'

import styles from './styles.css';
import spinner from './assets/spinner.svg';

const Loading = () => (
  <img className={ styles.img } data-test="loading" src={ spinner } />
)

export default Overlay(Loading)
