import React from "react";
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import Loading from "./";

describe("Loading", () => {

  const wrapper = () => {
    return shallow(
      <Loading />
    );
  };

  it("+++snapshot", () => {
    expect(toJson(wrapper())).toMatchSnapshot();
  });
});
