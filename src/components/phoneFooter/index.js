import React from 'react';
import PropTypes from 'prop-types';

import styles from './styles.css'

const PhoneFooter = ({ copyright }) => (
  <footer className={ styles.footer }>
    <div className={ styles.copyright }>{ copyright }</div>
  </footer>
)

PhoneFooter.propTypes = {
  copyright: PropTypes.string
};

PhoneFooter.defaultProps = {
  copyright: "My copyright"
};

export default PhoneFooter;
