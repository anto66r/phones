import React from "react";
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import PhoneFooter from "./";

describe("PhoneFooter", () => {
  let props;
  const wrapper = () => {
    return shallow(
      <PhoneFooter {...props} />
    );
  };

  beforeEach(() => {
    props = {
      copyright: ""
    };
  });

  it("+++snapshot, no copyright", () => {
    expect(toJson(wrapper())).toMatchSnapshot();
  });
  it("+++snapshot", () => {
    props.copyright = "aa"
    expect(toJson(wrapper())).toMatchSnapshot();
  });
});
