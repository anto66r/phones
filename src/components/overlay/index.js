import React from 'react';

import styles from './styles.css';

const Overlay = Component => (
  props => (
    <div className={ styles.overlayContainer }>
      <Component { ...props } />
      <div className={ styles.overlay }>
      </div>
    </div>
  )
)

export default Overlay;
