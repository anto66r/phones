import React from "react";
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';

import Overlay from "./";

describe("Overlay", () => {

  const wrapper = () => {
    return shallow(
      <Overlay />
    );
  };

  it("+++snapshot", () => {
    expect(toJson(wrapper())).toMatchSnapshot();
  });
});
