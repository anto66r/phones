import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as phones from 'actions/phones';
import * as popup from 'actions/popup';
import PhoneListContainer from 'components/phoneListContainer';
import PhoneHeader from 'components/phoneHeader';
import PhoneFooter from 'components/phoneFooter';
import './styles.css';

const App = ({
  phones,
  popup,
  actions
}) => {
  return (
    <React.Fragment>
      <PhoneHeader title="Our Phones" />
      <PhoneListContainer
        phones={ phones }
        actions={ actions }
        popup={ popup }
      />
      <PhoneFooter copyright="2018 FooRight" />
    </React.Fragment>
  )
};

const mapStateToProps = state => ({
  phones: state.phones,
  popup: state.popup
});

const mapDispatchToProps = dispatch => ({
  actions: {
    phones: bindActionCreators(phones, dispatch),
    popup: bindActionCreators(popup, dispatch)
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
