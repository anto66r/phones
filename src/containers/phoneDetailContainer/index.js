import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { keyBy } from 'lodash'

import * as popup from 'actions/popup';
import PhoneDetailComponent from 'components/phoneDetailComponent';

const mapStateToProps = state => {
  return {
    phones: keyBy(state.phones.phones, 'id')
  }
}

const mapDispatchToProps = dispatch => ({
  actions: {
    close: bindActionCreators(popup.close, dispatch)
  }
});

const PhoneDetailContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(PhoneDetailComponent);
 
export default PhoneDetailContainer;
