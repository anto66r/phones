import { Selector } from 'testcafe';

fixture `Phones`
  .page `http://localhost:8080`;

test('Click on item gives detail', async t => {
  await t
    .click('[data-test="item"]')
    .expect(Selector('[data-test="detail"]').count).eql(1)
});
test('Close detail view', async t => {
  await t
    .click('[data-test="item"]')
    .click('[data-test="close"]')
    .expect(Selector('[data-test="detail"]').count).eql(0)
});
