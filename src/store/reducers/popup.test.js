import popup from "./popup";
import * as types from '../actionTypes';

const initialState = {
  isPopupOpen: false
}

describe('Phones reducer', () => {
  it("should return initial state", () => {
    expect(popup(undefined, {})).toEqual(initialState);
  });
  it("should handle popup open", () => {
    expect(popup(undefined, {
      type: types.POPUP_OPEN,
      id: 5
    })).toEqual({
      isPopupOpen: true,
      id: 5
    });
  });
  it("should handle popup open", () => {
    expect(popup(undefined, {
      type: types.POPUP_CLOSE
    })).toEqual({
      isPopupOpen: false
    });
  });
});
