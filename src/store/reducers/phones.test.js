import phones from "./phones";
import * as types from '../actionTypes';

const initialState = {
  phones: [],
  loading: false,
  error: ""
}

describe('Phones reducer', () => {
  it("should return initial state", () => {
    expect(phones(undefined, {})).toEqual(initialState);
  });
  it("should handle load error", () => {
    expect(phones(undefined, {
      type: types.PHONES_LOAD_ERROR,
      error: {message: "error"}
    })).toEqual(
      { phones: [], loading: false, error: "error" }
    )
  });
  it("should handle phone load", () => {
    expect(phones(undefined, {
      type: types.PHONES_LOAD,
      phones: ['phones']
    })).toEqual(
      { phones: ['phones'], loading: false, error: "" }
    )
  });
  it("should handle phones LOADING", () => {
    expect(phones(undefined, {
      type: types.PHONES_LOADING
    })).toEqual(
      { phones: [], loading: true, error: "" }
    )
  })
});
