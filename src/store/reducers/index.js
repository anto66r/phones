import { combineReducers } from 'redux';

import phones from './phones';
import popup from './popup';

export default combineReducers({
  phones: phones,
  popup: popup
})
