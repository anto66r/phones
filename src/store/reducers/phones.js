import * as types from '../actionTypes';

const initialState = {
  phones: [],
  loading: false,
  error: ""
}

const phones = (state = initialState, action = {}) => {
  switch (action.type) {
    case types.PHONES_LOAD_ERROR:
      return { phones: [], loading: false, error: action.error.message }
    case types.PHONES_LOAD:
      return { phones: action.phones, loading: false, error: "" }
    case types.PHONES_LOADING:
      return { ...state, loading: true }
    default:
      return state
  }
}

export default phones;
