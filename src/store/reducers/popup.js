import * as types from '../actionTypes'

const initialState = {
  isPopupOpen: false
}

const popup = (state = initialState, action = {}) => {
  switch (action.type) {
    case types.POPUP_OPEN:
      return { isPopupOpen: true, id: action.id }
    case types.POPUP_CLOSE:
      return { isPopupOpen: false }
    default:
      return state
  }
}

export default popup;
