import * as types from '../actionTypes'
import axios from 'axios'

const phonesLoadSuccess = (data) => (
  {
    type: types.PHONES_LOAD,
    phones: data
  }
)
const phonesLoadFailure = (error) => (
  {
    type: types.PHONES_LOAD_ERROR,
    error: error
  }
)
export const load = () => {
  return async dispatch => {
    dispatch({type: types.PHONES_LOADING})
    try {
      const response = await axios.get('http://localhost:3000/phones');
      dispatch(phonesLoadSuccess(response.data));
    } catch (error) {
      dispatch(phonesLoadFailure("networkError"));
    }
  }
}
