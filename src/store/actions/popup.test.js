import { close, open } from './popup';
import * as types from '../actionTypes'

describe('Popup actions:', () => {
  it('open', () => {
    const expectedActions = {
      type: types.POPUP_OPEN,
      id: 5
    };
    expect(open(5)).toEqual(expectedActions)
  });
  it('close', () => {
    const expectedActions = {
      type: types.POPUP_CLOSE
    };
    expect(close()).toEqual(expectedActions)
  });
});
