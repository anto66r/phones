import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { load } from './phones';
import * as types from '../actionTypes'

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
var mock = new MockAdapter(axios);

describe('Phone actions:', () => {
  it('load success', () => {
    const data = ['phones'];
    mock.onGet('http://localhost:3000/phones').reply(200, data);

    const expectedActions = [
      {
        type: types.PHONES_LOADING
      },
      {
        type: types.PHONES_LOAD,
        phones: ['phones']
      }
    ]

    const store = mockStore({ phones: ['phones'] })
    return store.dispatch(load()).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  });
});
