import * as types from '../actionTypes';

export const open = (param) => ({
  type: types.POPUP_OPEN,
  id: param
})

export const close = () => ({
  type: types.POPUP_CLOSE
})
