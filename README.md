# Simple Phone List API/React app

## Running APP
### API
```
npm i
npm start
```

### React app
```
npm i
npm start
```

## Docker
### Dockerizing
To _dockerize_ API, run
```
cd phones/server
docker build -t phoneserver .
```

To _dockerize_ React app, run
```
cd phones
docker build -t phone .
```

### Running from Docker
Run API:
```
cd phones/server
run -d --name phoneserver -p 3000:3000
```

Run app:
```
cd phones
run -d --name phone -p 8080:8080
```

## Testing
### Unit
To run Jest testing suite:
```
cd phones
npm run test:unit
```
### End to end
To run example _e2e_ testing with testcafe:
```
cd phones
npm run test:e2e
```
