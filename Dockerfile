FROM node:9.4.0
RUN mkdir /phones
WORKDIR /phones
COPY . .
RUN npm install --quiet
CMD ["npm", "start"]

EXPOSE 8080
