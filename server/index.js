const express = require('express');
var cors = require('cors');
const phones = require('./phones.json')

const app = express();
app.use(cors());

app.listen(3000, function () {
 console.log("App listening on port 3000!");
});
app.get("/phones", function (req, res) {
  setTimeout(()=> {
    res.set('Content-Type', 'application/json');
    res.send(JSON.stringify(phones));
  }, 2000);  
});
